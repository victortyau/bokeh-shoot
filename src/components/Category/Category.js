import React from 'react';
import './Category.css';
import Section from '../Section/Section';

class Category extends React.Component {
    render() {
        const section_list = ["diode", "resistor", "transistor", "capacitor"].map(function(section){
                           return <Section value={section} />;
                        });

        return <div className="category-container">
                    <div className="category-header" >
                        <h1>Categories</h1>
                    </div>
                    <div  className="category-section">
                        { section_list }
                    </div>
               </div>;
    }
}

export default Category;