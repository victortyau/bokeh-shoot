import React from 'react';
import './Square.css';

class Square extends React.Component {

    render() {
    return <div className="square-container"><h1>{ this.props.value }</h1></div>;
    }

}

export default Square;