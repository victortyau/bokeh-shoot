import React from 'react';
import './Footer.css';

class Footer extends React.Component {
    render() {
        return <div className="footer-container">
            <div className="footer-content">
                <span>Copyright © Bokeh Shoot 2020 by victortyau</span>
            </div>
        </div>
    }
}

export default Footer;