import React from 'react';
import './Section.css';

class Section extends React.Component {
    render() {
    return <div className="section-container">{ this.props.value }</div>
    }
}

export default Section;