import React from 'react';
import './Menu.css';

class Menu extends React.Component {

    render() {
        return <div className="menu-container">
            <nav >
                <ul className="menu-nav">
                    <li><a href="javascript:void(0);">Inicio</a></li>
                    <li><a href="javascript:void(0);">52 Semanas</a></li>
                    <li><a href="javascript:void(0);">Sobre Mi</a></li>
                    <li><a href="javascript:void(0);">Contacto</a></li>
                </ul>
            </nav>
        </div>;
    }
}

export default Menu;