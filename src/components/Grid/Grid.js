import React from 'react';
import './Grid.css'
import '../Square/Square';
import Square from '../Square/Square';

class Grid extends React.Component {

    render() {
        
        var photos = ["bokeh.jpg", "lego.jpg", 
                      "wacom.jpg", "pickit4.jpg", 
                      "fedora.jpg", "centos.jpg",
                      "kicad.jpg", "fedpkg.jpg",
                      "podman.jpg"];
      
        const square_list = photos.map(function(photo){
            return <Square value={photo} />;
        }); 

        return <div className="grid-container">
                    { square_list }
                </div>;
    }

}

export default Grid;