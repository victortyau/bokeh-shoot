import React from 'react';
import './Title.css';

class Title extends React.Component {
    // eslint-disable-next-line react/require-render-return
    render() {
        return <div className="title-container">
                <h1 className="title-message">
                    !Bokeh Shoot!
                </h1>
               </div>;
    }
}

export default Title;
