import React from 'react';
import './Home.css';
import Title from '../Title/Title';
import Menu from '../Menu/Menu';
import Grid from '../Grid/Grid';
import Category from '../Category/Category';
import Footer from '../Footer/Footer';

class Home extends React.Component {
    render() {
        return <div>
            <Title />
            <Menu />
            <Grid />
            <Category />
            <Footer />
        </div>;
    }
}

export default Home;